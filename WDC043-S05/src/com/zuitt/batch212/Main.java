package com.zuitt.batch212;

import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {


        /*System.out.println(phonebook);*/

        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        ArrayList<String> numbers1 = new ArrayList<>();
        numbers1.add("+639152468596");
        numbers1.add("+639228547963");
        ArrayList<String> addresses1 = new ArrayList<>();
        addresses1.add("Quezon City");
        addresses1.add("Makati City");
        contact1.setNumbers(numbers1);
        contact1.setAddresses(addresses1);

        contact1.showContact();

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        ArrayList<String> numbers2 = new ArrayList<>();
        numbers2.add("+639162148573");
        numbers2.add("+9173698541");
        ArrayList<String> addresses2 = new ArrayList<>();
        addresses2.add("Caloocan City");
        addresses2.add("Pasay City");
        contact2.setNumbers(numbers2);
        contact2.setAddresses(addresses2);

        contact2.showContact();

        Phonebook phonebook = new Phonebook();
        ArrayList<String> allContacts = new ArrayList<>();
        allContacts.add(String.valueOf(contact1));
        allContacts.add(String.valueOf(contact2));



        phonebook.setContacts(allContacts);

        /*phonebook.showAllContacts();*/
        /*phonebook.showContact();*/
    }



}
