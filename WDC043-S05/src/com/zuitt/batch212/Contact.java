package com.zuitt.batch212;

import java.util.ArrayList;

public class Contact {

    public String name;
    public ArrayList<String> numbers = new ArrayList<>();
    public ArrayList<String> addresses = new ArrayList<>();

//  CONSTRUCTOR
    public Contact() {}

    public Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }


//  GETTER
    public String getName() {
        return name;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }



//  SETTER
    public void setName(String name) {
        this.name = name;
    }

    public void setNumbers(ArrayList<String> numbers) {
        this.numbers = numbers;
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
    }


//  METHOD

    public void showContact() {
        System.out.println(this.name);
        System.out.println("---------------");
        System.out.println(this.name + " has the following registered numbers:");
            for (String nums: numbers) {
                System.out.println(nums);
            }
        System.out.println("------------------------------");
        System.out.println(this.name + " has the following registered addresses:");
            /*for (String adds: addresses) {
                *//*System.out.println(adds);*//*

            }*/
        System.out.println("My Home is in " + addresses.get(0));
        System.out.println("My Workplace is in " + addresses.get(1));
        System.out.println("==============================");
        }


}
